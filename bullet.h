#ifndef BULLET
#define BULLET

typedef struct Bullet
{
    float damage;
    float speed;
    float x, y;
    int move_x, move_y;
    unsigned id;
} Bullet;

#endif
