#ifndef GAME
#define GAME

#include "creature.h"

typedef struct Game
{
    Creature *creatures;
    Bullet *bullets;
    unsigned last_id;
    unsigned *to_delete;
} Game;



Game *create_game()
{
    Game *game = malloc(sizeof(Game));

    return game;
}



Creature * add_creature(Game *game)
{
    int size = sizeof(game->creatures) / sizeof(Creature);
    size += sizeof(Creature);
    game->creatures = realloc(game->creatures, size);

    game->last_id++;
    Creature *creature = create_creature(0, 0, game->last_id);
    Creature *ptr = game->creatures + size - 1;
    ptr = creature;

    return creature;
}



void delete_creature(Game *game, unsigned id)
{
    int size = sizeof(game->creatures) / sizeof(Creature);
    Creature *ptr = game->creatures;
    for (int i=0; i<size; i++)
    {
        if (ptr->id == id)
        {
            break;
        }
    }
    destroy_creature(ptr);
    ptr = game->creatures + size - 2;
    game->creatures = realloc(game->creatures, size-1);
}



void game_loop()
{

}



void update_creatures(Game *game)
{
    int size = sizeof(game->creatures) / sizeof(Creature);
    for (int i=0; i<size; i++)
    {
        update_creature(&game->creatures[i]);
    }
}



void update_game(Game *game)
{
    update_creatures(game);
}

#endif
