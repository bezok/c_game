#include <stdlib.h>
#include <Python.h>

#ifndef CREATURE
#define CREATURE

typedef struct Creature
{
    float hp, max_hp;
    float damage;
    float speed;
    float x, y;
    int move_x, move_y;
    unsigned id;
} Creature;



Creature * create_creature(float x, float y, unsigned id)
{
    Creature *creature = malloc(sizeof(Creature));

    PyObject *module, *hp, *damage, *speed;
    module = PyImport_ImportModule("creature");
    hp = PyObject_GetAttrString(module, "hp");
    damage = PyObject_GetAttrString(module, "damage");
    speed = PyObject_GetAttrString(module, "speed");

    creature->damage = PyFloat_AsDouble(damage);
    creature->hp = PyFloat_AsDouble(hp);
    creature->max_hp = creature->hp;
    creature->speed = PyFloat_AsDouble(speed);
    creature->x = x;
    creature->y = y;
    creature->move_x = x;
    creature->move_y = y;
    creature->id = id;

    return creature;
}



void destroy_creature(Creature *creature)
{
    free(creature);
}



void hit_creature(Creature *creature, float damage)
{
    creature->hp -= damage;
}



void attack_creature(Creature *self, Creature *attacked)
{
    hit_creature(attacked, self->damage);
}



void move_creature(Creature *creature)
{
    int x_dist = (int)creature->x - creature->move_x;
    int y_dist = (int)creature->y - creature->move_y;

    if (x_dist)
    {
        if (x_dist < 0)
        {
            creature->x += creature->speed;
        }
        else
        {
            creature->x -= creature->speed;
        }
    }

    if (y_dist)
    {
        if (y_dist < 0)
        {
            creature->y += creature->speed;
        }
        else
        {
            creature->y -= creature->speed;
        }
    }
}



void update_creature(Creature *creature)
{
    move_creature(creature);

    if (creature->hp < creature->max_hp)
    {
        creature->hp += 1;
    }
}



void set_move_creature(Creature *creature, int x, int y)
{
    creature->move_x = x;
    creature->move_y = y;
}

#endif
