TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
LIB += websockets

SOURCES += main.c

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/release/ -lpython3.5m
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/debug/ -lpython3.5m
else:unix: LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/ -lpython3.5m

INCLUDEPATH += $$PWD/../../../../usr/include/python3.5m
DEPENDPATH += $$PWD/../../../../usr/include/python3.5m

DISTFILES += \
    creature.py \
    network.py

HEADERS += \
    creature.h \
    game.h \
    bullet.h


